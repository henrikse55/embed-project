#include "SensorStatus.h"

SensorStatus::SensorStatus(LayerHandler* layer, LCD_DISCO_F746NG* lcd) {
    TempValues = new LimitedLinkedList<float, short int>(25);
    SoundValues = new LimitedLinkedList<float, short int>(25);
    LightValues = new LimitedLinkedList<float, short int>(25);

    bus->RegisterEvent(VIDEO_TOGGLE, callback(this, &SensorStatus::ToggleUpdate));
    bus->RegisterEvent(HTTP_PARSER, callback(this, &SensorStatus::ToggleUpdate));

    thread.start(callback(this, &SensorStatus::Update));
    this->lcd = lcd;
    video = layer;
}

float SensorStatus::GetTemp() {
    AnalogIn sensor(TEMP_SENSOR);
    return (float)VoltageToC(sensor.read());
}

float SensorStatus::GetSound() {
    AnalogIn sound(SOUND_SENSOR);
    float high = 0;
    float low = 0;
    for(int i = 0; i < 512; i++) {
        float read = sound.read();
        if(read > high) high = read;
        if(read < low || low == 0) low = read;
    }
    return 20 * log(high/low);
}

float SensorStatus::GetLight() {
    AnalogIn sensor(LIGHT_SENSOR);
    return sensor.read();
}

void SensorStatus::Update() {
    while(true) {

        wait(0.3);
        float temperature = GetTemp();
        float volume = GetSound();
        float lightVal = GetLight();

        char* TemperatureVal = GetChar("%.2fC\0", temperature);
        char* SoundDb = GetChar("%.0f dB\0", volume);
        char* LightSense = GetChar("%.2f\0", lightVal);

        if(!VideoToggle) {
            UpdateData(0, TemperatureVal);
            UpdateData(2, SoundDb);
            UpdateData(3, LightSense);
        }

        /* Avg, Max, Min */
        TempValues->push_back(temperature);
        SoundValues->push_back(volume);
        LightValues->push_back(lightVal);

        float TempSum = 0, TempMax = 0, TempMin = 500;
        float SoundSum = 0, SoundMax = 0, SoundMin = 500;
        float LightSum = 0, LightMax = 0, LightMin = 500;

        float* items = TempValues->GetAllValues();
        GetStats(items, TempValues->Count(), &TempSum, &TempMax, &TempMin);
        delete items;

        items = SoundValues->GetAllValues();
        GetStats(items, SoundValues->Count(), &SoundSum, &SoundMax, &SoundMin);
        delete items;

        items = LightValues->GetAllValues();
        GetStats(items, LightValues->Count(), &LightSum, &LightMax, &LightMin);
        delete items;

        char* TempAvgChar = GetChar("%.2fC" , TempSum);
        char* TempMaxChar = GetChar("%.2fC", TempMax);
        char* TempMinChar = GetChar("%.2fC",  TempMin);

        char* SoundAvgChar = GetChar("%.0f dB", SoundSum);
        char* SoundMaxChar = GetChar("%.0f dB", SoundMax);
        char* SoundMinChar = GetChar("%.0f dB", SoundMin);

        char* LightAvgChar = GetChar("%.2f", LightSum);
        char* LightMaxChar = GetChar("%.2f", LightMax);
        char* LightMinChar = GetChar("%.2f", LightMin);

        if(!VideoToggle){
            UpdateData(7, TempAvgChar);
            UpdateData(10, TempMaxChar);
            UpdateData(13, TempMinChar);

            UpdateData(8, SoundAvgChar);
            UpdateData(11, SoundMaxChar);
            UpdateData(14, SoundMinChar);

            UpdateData(9, LightAvgChar);
            UpdateData(12, LightMaxChar);
            UpdateData(15, LightMinChar);
        }

        if(!VideoToggle) {
            items = LightValues->GetAllValues();
            video->DrawGraphAt(items, LightValues->Count(), (lcd->GetXSize() - 200), (lcd->GetYSize() / 2) - 30, 150);
            delete items;
        }
    }
}

void SensorStatus::ToggleUpdate(message_t* message) {
    VideoToggle = !VideoToggle;
}


