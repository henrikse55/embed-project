#include "LayerHandler.h"

//*******************************************************/
// Constructors and Desstructors
//*******************************************************/
LayerHandler::LayerHandler(LCD_DISCO_F746NG& lcd) {
    this->lcd = lcd;
    bus->RegisterEvent(VIDEO_UPDATE, callback(this, &LayerHandler::Notify));
}

LayerHandler::~LayerHandler() {
    //TODO: Delete pointers
}

//*******************************************************/
// Public Member Functions
//*******************************************************/

void LayerHandler::ChangeLayer(const size_t layer) {
    if(this->layers.size() >= layer) {
        this->current = layer;
        this->lcd.Clear(LCD_COLOR_BLACK);
        this->Draw();
    }
}

size_t LayerHandler::AddLayer(Render* rend, size_t size) {
    Layer lay {
        rend,
        size
    };
    this->layers.push_back(lay);
    return this->layers.size()-1;    
}

void LayerHandler::Draw() {
    if(layers.size() > 0) {
        Layer layer = this->layers[this->current];
#ifdef TRACE
        logger->Log(Level::NOTICE, "Large layer draw with %d elements", layer.size);
#endif
        for(size_t i = 0; i < layer.size; i++) {
            Render render = layer.renders[i];
            this->DrawRender(&render);
        }
    }
}

void LayerHandler::CallPressed(const TS_StateTypeDef touch) {
    if(!layers.empty()) {
        Layer layer = this->layers[this->current];
        for(size_t i = 0; i < layer.size; i++) {
            Render render = layer.renders[i];
            for(size_t x = 0; x < touch.touchDetected; x++) {
                if(touch.touchX[x] >= render.x && touch.touchX[x] <= render.x + render.width) {
                    if(touch.touchY[x] >= render.y && touch.touchY[x] <= render.y + render.height){
                        if(render.handle != 0) render.handle(i);
                    }    
                }
            }
        }    
    }
}

//TODO Generalize SetVisibileBytitle and SetVisibleByIndex - Day almost loock identical
void LayerHandler::SetVisibleByTitle(const char* title, bool value) {
    if(!layers.empty()) {
        Layer layer = layers[current];
        for(size_t i = 0; i < layer.size; i++) {
            Render* render = &layer.renders[i];
            if(strcmp(title, render->title) == 0)
                (render->visible) = value;
        }    
    }    
}

void LayerHandler::SetVisibleByIndex(size_t index, bool value) {
    if(!layers.empty()) {
        Layer layer = layers[current];
        Render* render = &layer.renders[index];
        if(render->visible != value) {
            render->visible = value;
            this->DrawRender(render);
        }
    }      
}

void LayerHandler::MoveToPos(Render* render, const size_t x, const size_t y) {
    //TODO: Could I do this another way that's more effecient?
    //Remove "old" location
    render->visible = false;
    this->DrawRender(render);
    
    //Draw new version
    render->x = x;
    render->y = y;
    render->visible = true;
    this->DrawRender(render);
    
    bool closeRenderDone = false;
    
    Layer layer = layers[this->current];
    //Go through each of layer's renders
    for(size_t i = 0; i < layer.size; i++) {
        //No need to do the loop di loops if nothing would be rendered anyway
        if(layer.renders[i].visible) {
            //Go through each x position of the render (with an offset)
            for(size_t rx = render->x - 45; rx != (render->x + render->width) + 45; rx++) {
                
                //Stop the loop in case It's already been redrawn
                if(closeRenderDone) {
                    closeRenderDone = false;
                    break;    
                }
                
                //Go though each y position of the render (with an offset)
                for(size_t ry = render->y - 45; ry != (render->y + render->height)+ 45; ry++) {
                    Render* closeRender = 0;
                    if((closeRender = &layer.renders[i]) != render) {
                        
                        //Validate that it's inside of the range
                        if(rx >= closeRender->x && rx <= closeRender->x + closeRender->width) {
                            if(ry >= closeRender->y && ry <= closeRender->y + closeRender->height) {
                                this->DrawRender(closeRender);
                                closeRenderDone = true;
                                break;
                            }
                        }
                    }
                }    
            }
        }
    }
    
    //Redraw the moving box on top
    this->DrawRender(render);
}

bool LayerHandler::UpdateContent(const size_t index, const char* content) {
    //Make sure there's a layer to render
    if(!layers.empty()) {
        Layer layer = this->layers[this->current];
        Render* render = &layer.renders[index];
        if(!this->IsEqual(render->content, content)) {
            delete render->content;
            render->content = (char*)malloc(sizeof(char) * std::string(content).size()+1);
            memset(render->content, 0, sizeof(char) * std::string(content).size()+1);
            memcpy(render->content, content, sizeof(char) * std::string(content).size());
            this->DrawRender(render);
            return true;
        }
    }
    return false;
}

Render* LayerHandler::FindRenderByPos(const uint16_t x, const uint16_t y) {
    if(!layers.empty()) {
        Layer layer = this->layers[this->current];
        for(size_t i = 0; i < layer.size; i++) {
            Render* render = &layer.renders[i];
            if(x <= render->x + render->width && x >= render->x) {
                if(y <= render->y + render->height && y >= render->y) {
                    return render;
                }
            }    
        }
    }
    
    return 0;
}

void LayerHandler::DrawGraphAt(float* values, const size_t size, const size_t x, const size_t y, const size_t area) {
    //Preperation
    size_t endY = y + area;
    
    //Remove old graph
    this->lcd.SetTextColor(LCD_COLOR_BLACK);
    this->lcd.FillRect(x, y, area + 5, area + 5);
    this->lcd.SetTextColor(LCD_COLOR_WHITE);
    
    //Draw graph borders
    this->lcd.DrawLine(x, y, x, endY);
    lcd.DrawLine(x, endY, x + area, endY);
    
    //Helper varibables
    size_t offset = area / size;
    size_t oldY = y + (area / 2), oldX = x;
    
    //Draw graph Lines
    for(size_t i = 0; i < size; i++) {
        float value = values[i];
        size_t YPos = endY - (area * value);
        lcd.DrawLine(oldX, (i == 0 ? YPos : oldY), (oldX + offset), YPos);
        
        //Advance the positions
        oldY = YPos;
        oldX += offset;
    }
}

void LayerHandler::Notify(message_t *message) {
    LOG("Updating, %p", message);
    LOG("param, %p", message->param);
    video_t* update = (video_t*)message->param;
    UpdateContent(update->id, update->data);
    delete update;
}

//*******************************************************/
// Private Member Functions
//*******************************************************/

inline
void LayerHandler::DrawRender(Render* render) {
    //If not set to be shown cover the boxes section
    if(!render->visible){
        //Covers entire box, don't use RemoveOldText here
        this->lcd.SetTextColor(LCD_COLOR_BLACK);
        this->lcd.FillRect(render->x, render->y-2, render->width + 1, render->height + 3);
        this->lcd.SetTextColor(LCD_COLOR_WHITE);
    } else {
        //Draw the box
        this->lcd.DrawRect(render->x, render->y, render->width, render->height);

        //Change font and draw the title
        this->lcd.SetFont(&Font8);
        if(render->title != 0)
            this->lcd.DisplayStringAt(render->x + 3, render->y -2, (uint8_t *)render->title, LEFT_MODE);

        //change font and draw the content
        this->lcd.SetFont(&Font12);
        if(render->content != 0) {
            this->RemoveOldText(render->x, render->y, render->width, render->height);
            this->lcd.DisplayStringAt(render->x + 10, render->y + (render->height / 2), (uint8_t*)render->content, LEFT_MODE);
        }
    }
}

bool LayerHandler::IsEqual(const char* first, const char* second) {
    //Go through as long as no string is at the end
    return std::strcmp(first, second) == 0;
}

void LayerHandler::RemoveOldText(size_t x, size_t y, size_t width, size_t height) {
    this->lcd.SetTextColor(LCD_COLOR_BLACK);
    this->lcd.FillRect(x+1, y + (height / 2), width - 1, height / 2);
    this->lcd.SetTextColor(LCD_COLOR_WHITE);
}
