#include "Network/ServerSocket.h"

ServerSocket::ServerSocket(size_t port, const std::string& event) {
    LOG("Initializing Server on %d", port);
    this->port = port;
    bus->RegisterEvent("network-change", callback(this, &ServerSocket::Notify));
    bus->RegisterEvent(event + "-response", callback(this, &ServerSocket::RespondToSocket));
    this->event = event;
    MSG("Done");
}

//ServerSocket::~ServerSocket() {
//    delete thread;
//    delete interface;
//}

void ServerSocket::Notify(message_t *message) {
    if((int)message->param == 1){
        interface = NetworkInterface::get_default_instance();

        nsapi_error_t error = server.open(interface);
        if(error != 0) {
            CLOG(Level::WARNING, "Failed to opeen tcp server on %p | error: %d", interface, error);
            return;
        }
        LOG("TCP Server have been opened on the interface %p", interface);

        error = server.bind(port);
        if(error != 0) {
            CLOG(Level::WARNING, "Failed to bind port %d | error: %d", port, error);
            return;
        }
        MSG("Successfully bound server");

        error = server.listen(20);
        if(error != 0) {
            CLOG(Level::WARNING, "Failed to start listening on port: %d | error: %d", port, error);
            return;
        }

        server.set_blocking(false);

        running = true;
        thread = new Thread();
        thread->start(callback(this, &ServerSocket::Listen));
        MSG("Starting Listening thread");
    } else {
        MSG("Shutting down socket server!");
        running = false;
        for(auto it = connections.begin(); it != connections.end(); ++it) {
            Connector* connector = *it;
            if(connector->socket != nullptr) {
                connector->socket->close();
                connections.erase(it--);
            }
        }
        server.close();
        thread->join();
        delete thread;
    }
}

void ServerSocket::Listen() {
    MSG("Socket Listening thread is now running");
    while(running) {
        nsapi_error_t error;
        TCPSocket* client = server.accept(&error);
        if(error == 0) {
            MSG("client connected");
            client->set_blocking(false);
            MSG("unblocked");
            Connector* conn = new Connector{
                    0,
                    client
            };
            connections.push_back(conn);
            MSG("Pushed to queue");
        }

        for(auto it = connections.begin(); it != connections.end(); ++it) {
            Connector* connector = *it;

            if(connector->Pending) {
                continue;
            }

            char* buffer = (char*)malloc(sizeof(char) * 1024);
            memset(buffer, 0, sizeof(char) * 1024);

            nsapi_size_or_error_t sizeOrError = connector->socket->recv(buffer, 1024);
            if(sizeOrError > 0) {
                std::string value;
                if(connector->message != nullptr){
                    value += connector->message;
                }
                value += buffer;

                char* message = new char[value.size()+1];
                memset(message, 0, value.size()+1);
                memcpy(message, value.data(), value.size());
                connector->Pending = true;

                Connection* conn = new Connection {
                        message,
                        connector->socket
                };

                MSG("Passing to be parsed data to event queue");
                message_t* msg = bus->CreateMessage();
                msg->event = event.data();
                msg->param = conn;
                bus->Add(msg);
                MSG("Done pushing data");
            }

            if(sizeOrError == 0){
                MSG("Client seems to have disconnected!");
                connector->socket->close();
                connections.erase(it--);
            }
            delete buffer;
        }
    }
}

void ServerSocket::RespondToSocket(message_t* message) {
    Connection* conn = (Connection*)message->param;
    std::string msg(conn->message);
    MSG("Writing message to socket...");
    while(msg.size() > 0){
        int sent = conn->socket->send(msg.data(), msg.size());
        if(sent > 0) {
            msg = msg.substr(sent);
        }
    }
    for(auto it = connections.begin(); it != connections.end(); ++it) {
        if(conn->socket == ((Connector*)*it)->socket) {
//            ((Connector*)*it)->socket->close();
//            connections.erase(it--);
            ((Connector*)*it)->Pending = false;
        }
    }

    delete conn->message;
    delete conn;
}
