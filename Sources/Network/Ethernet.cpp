#include "Network/Ethernet.h"

void Ethernet::Initialize() {
    logger->Log("Initializing eth");

    interface = NetworkInterface::get_default_instance();
    interface->connect();
    StateWatch.start(callback(this, &Ethernet::StateChange));

    std::string ip(interface->get_ip_address());

    message_t* message = bus->CreateMessage(NETWORK_IP);

    char* messageIp = (char*)malloc(sizeof(char) * ip.size()+1);
    memset(messageIp, 0, sizeof(char) * ip.size()+1);
    memcpy(messageIp, ip.c_str(), sizeof(char) * ip.size());

    message->param = messageIp;
    bus->Add(message);
}

void Ethernet::StateChange() {
    while(true) {
        int param = interface->get_connection_status();
        if(param == NSAPI_STATUS_CONNECTING && Connected){
            Connected = false;
            message_t* message = bus->CreateMessage(NETWORK_CHANGE);
            message->param = (void *) 0;
            bus->Add(message);
        }

        if(param == NSAPI_STATUS_GLOBAL_UP && !Connected) {
            Connected = true;
            message_t* message = bus->CreateMessage(NETWORK_CHANGE);
            message->param = (void *) 1;
            bus->Add(message);
        }

        wait(5);
    }
}

NetworkInterface* Ethernet::GetInterface() {
    return interface;
}

Ethernet* Ethernet::GetInstance() {
    static Ethernet eth;
    return &eth;
}

Ethernet::~Ethernet() {
    delete interface;
}