#include "Json.h"
//Prepare the json object
Json::Json(JsonValue* items, unsigned int count) {
    this->items = items;  
    this->size = count;  
}

// Clean up the memory
Json::~Json() {
    //delete this->items;    
}

//Calculare the approximate size required
size_t Json::GetBufferSize() {
    size_t buffer = 6 * this->size;
    for(size_t i = 0; i < this->size; i++) {
        JsonValue val = this->items[i];
        buffer += strlen(val.name);
        buffer += strlen(val.value);
    }
    return buffer;
}


//Generate the json data
char* Json::GetJson() {
    char* buffer = new char[this->GetBufferSize()];
    sprintf(buffer, "{");
    
    //Go though every json value
    for(unsigned int i = 0; i < this->size; i++) {
        JsonValue val = this->items[i];
        if (i != this->size - 1) {
            sprintf(buffer, R"(%s"%s":"%s",)", buffer, val.name, val.value);
        } else {
            sprintf(buffer, R"(%s"%s":"%s")", buffer, val.name, val.value);
        }
    }
    
    sprintf(buffer, "%s}", buffer);
    return buffer;
}
