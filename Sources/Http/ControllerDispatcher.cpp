#include "Http/ControllerDispatcher.h"

ControllerDispatcher::ControllerDispatcher() {
    bus->RegisterEvent(HTTP_PARSER, callback(this, &ControllerDispatcher::Notify));
    bus->RegisterEvent("controller-register", callback(this, &ControllerDispatcher::RegisterNotify));
}

void ControllerDispatcher::Notify(message_t *message) {
    LOG("Starting Parsing, connector: %p", message->param);
    Connection* connector = (Connection*)message->param;

    LOG("Parsing Header %p", connector->message);
    HttpHeader* header = HttpHeader::Parse(connector->message);

    MSG("Making request and response objects");
    HttpRequest* request = new HttpRequest();
    request->Header = header;

    HttpResponse* httpResponse = new HttpResponse();
    httpResponse->Header.AddEntry("Server", "HF5 Embedded Controller");
    httpResponse->Header.AddEntry("Connection", "keep-alive");

    MSG("Making Context");
    HttpContext context;
    context.request = request;
    context.response = httpResponse;
    context.socket = connector->socket;

    ActionResult result {
        Code::NotFound
    };

    std::string path = request->Header->Path();
    if(path == "/") {
        path = "/home";
    }

    LOG("Locating Controller for path %s", path.data());
    for(Controller* controller : Controllers) {
        //TODO: Refactor code below for higher effeciency
        std::string base("/" + std::string(controller->GetBaseRoute()));
        LOG("Base %s", base.data());
        if(path.find(base) == 0) {
            LOG("Controller Base: %s", base.data());
            LOG("Request Path: %s", path.data());
            std::string action = "index";
            if(path.size() > base.size()) {
                action = path.substr(base.size()+1);
            }
            LOG("Controller Found %s", action.data());
            Callback<Action()>* act = controller->GetAction(action);
            if(act != nullptr) {
                httpResponse->Header.AddEntry("Content-Type", "text/html");
                controller->SetContext(&context);
                MSG("Calling action...");
                result = (*act)();
                break;
            }else{
                CLOG(Level::FATAL,"Action pointer was not found! %s", controller->GetBaseRoute());
            }
        }else{
            LOG("NO MATCH %s = %s", base.data(), path.data());
        }
    }

    if(!context.DontSend) {
        size_t size = context.response->body.str().size();
        context.response->Header.AddEntry("Content-Length", std::to_string(size));
        std::string response = context.FormResult(result);

        MSG("Preparing response to socket");

        char* data = new char[response.size()+1];
        memset(data, 0, response.size()+1);
        memcpy(data, response.data(), response.size());

        Connection* connection = new Connection();
        connection->message = data;
        connection->socket = connector->socket;

        MSG("Making bus message");
        message_t* msg = bus->CreateMessage(HTTP_PARSER_RESPONSE);
        msg->param = connection;
        bus->Add(msg);
    }

    message_t* video_resume = bus->CreateMessage(VIDEO_TOGGLE);
    bus->Add(video_resume);

    //Clean up
    delete header;
    delete request;
    delete httpResponse;
}

void ControllerDispatcher::RegisterNotify(message_t* message) {
    Controller* controller = (Controller*)message->param;
    if(controller != nullptr){
        LOG("Registering controller from %p", controller);
        Controllers.push_back(controller);
    }else{
        CLOG(Level::WARNING,"Cannot register controller at %p", controller);
    }
}