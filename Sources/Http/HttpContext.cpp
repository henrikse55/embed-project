#include "Http/HttpContext.h"

std::string HttpContext::FormResult(const ActionResult& result) {
    std::stringstream content;

    MSG("response base");
    content << "HTTP/1.1 " << CodeToString(result.code) << "\r\n";

    MSG("Adding other header values");
    for(const auto& entry : *response->Header.GetEntries()){
        content << entry.first << ": " << entry.second << "\r\n";
    }

    //TODO Remvoe this when chunking is enabled, this is sent seperated from the header, I think
    MSG("Appending Body");
    content << "\r\n\r\n";
    content << response->body.str();
    content << "\r\n\r\n";

    return content.str();
}