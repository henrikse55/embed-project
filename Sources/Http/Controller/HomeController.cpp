#include "Http/Controller/HomeController.h"
#include "Encoding.h"

HomeController::HomeController(const std::string &base) : Controller(base) {
    //Register controller actions
    RegisterAction("index", callback(this, &HomeController::Index));
    RegisterAction("login", callback(this, &HomeController::Login));

    //Invoke the registration method of the dispatcher
    message_t* message = bus->CreateMessage(CONTROLLER_REGISTER);
    message->param = this;
    bus->Add(message);
}

// /home/index
ActionResult HomeController::Index() {
    auto& body = GetContext()->response->body;
    body << "<h1>HELLO WORLD</h1>";
    return Ok();
}

// home/login
ActionResult HomeController::Login() {
    auto& body = GetContext()->response->body;

    //Check if basic auth is present at all
    const char* authPtr = GetContext()->request->Header->GetEntry("Authorization");
    if(authPtr == nullptr){
        GetContext()->response->Header.AddEntry("WWW-Authenticate", "Basic realm=\"Login Required here!\"");
        return UnAuthorized();
    } else {
        //Validate the given basic auth
        std::string auth(authPtr);
        auth = auth.substr(auth.find_last_of(' ')+1);
        std::string login = Encoding().Decode(auth);
        if(login.find("admin:admin") != std::string::npos){
            body << "<h2>OMFG HELLO WORLD!</h2>";
            return Ok();
        }

        GetContext()->response->Header.AddEntry("WWW-Authenticate", "Basic realm=\"Login Required here!\"");
        return UnAuthorized();
    }
}
