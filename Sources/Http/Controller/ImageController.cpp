#include "Http/Controller/ImageController.h"
#include <math.h>

ImageController::ImageController(const std::string &base) : Controller(base) {
    //Register controller actions
    RegisterAction("index", callback(this, &ImageController::index));
    RegisterAction("data", callback(this, &ImageController::Data));

    //Register controller in the dispatcher
    message_t* message = bus->CreateMessage(CONTROLLER_REGISTER);
    message->param = this;
    bus->Add(message);
}

ActionResult ImageController::index() {
    GetContext()->DontSend = true;

    size_t XSize = BSP_LCD_GetXSize();
    size_t YSize = BSP_LCD_GetYSize();

    bmpfile_magic magic;
    magic.magic[0] = 'B';
    magic.magic[1] = 'M';

    bmpfile_header header;
    header.bmp_offset = sizeof(bmpfile_magic)
                        + sizeof(bmpfile_header) + sizeof(bmpfile_dib_info);
    header.file_size = header.bmp_offset
                       + ((XSize * YSize) * 3 + 32 % 4) * (XSize * YSize);

    bmpfile_dib_info dib_info;
    dib_info.header_size = sizeof(bmpfile_dib_info);
    dib_info.width = XSize;
    dib_info.height = YSize;
    dib_info.num_planes = 1;
    dib_info.bits_per_pixel = 32;
    dib_info.compression = 0;
    dib_info.bmp_byte_size = 0;
    dib_info.hres = 2835;
    dib_info.vres = 2835;
    dib_info.num_colors = 0;
    dib_info.num_important_colors = 0;

    GetContext()->socket->send(&magic, sizeof(magic));
    GetContext()->socket->send(&header, sizeof(header));
    GetContext()->socket->send(&dib_info, sizeof(dib_info));

    GetContext()->socket->set_blocking(true);
    for(size_t y = YSize; y > 0; y--) {
        std::vector<size_t> pixels;
        for(size_t x = 0; x < XSize; x++) {
            size_t value = BSP_LCD_ReadPixel(x,y);
            pixels.push_back(value);
        }
        GetContext()->socket->send(&pixels[0], 4 * pixels.size());
    }
    GetContext()->socket->set_blocking(false);

    GetContext()->socket->close();
    return Ok();
}

ActionResult ImageController::Data() {
    GetContext()->DontSend = true;

    auto sendFunc = [&](const std::string& data){
        std::string chunk = data;
        GetContext()->socket->set_blocking(true);
        while(!chunk.empty()){
            int sent = GetContext()->socket->send(chunk.data(), chunk.size());
            if(sent > 0){
                chunk = chunk.substr(sent);
            }
        }
        GetContext()->socket->set_blocking(false);
    };

    size_t XSize = BSP_LCD_GetXSize();
    size_t YSize = BSP_LCD_GetYSize();

    std::stringstream content;

    MSG("response base");
    content << "HTTP/1.1 " << CodeToString(Code::OK) << "\r\n";

    GetContext()->response->Header.AddEntry("Transfer-Encoding", "chunked");

    for(const auto& entry : *GetContext()->response->Header.GetEntries()){
        content << entry.first << ": " << entry.second << "\r\n";
    }

    content << "\r\n";

    std::string chunk = content.str();
    sendFunc(chunk);

    std::string svgStart("<svg width=\"" + std::to_string(XSize) + "\" height=\"" + std::to_string(YSize) + "\" style=\"background-color: black\">\r\n");
    std::stringstream svgSend;
    svgSend << std::hex << svgStart.size() << "\r\n";
    svgSend << svgStart;
    svgSend << "\r\n";
    sendFunc(svgSend.str());

    MSG("Sending lines");
    std::vector<std::string> lines;
    for(size_t x = 0; x < XSize; x++) {
        for(size_t y = 0; y < YSize; y++) {
            size_t value = (((BSP_LCD_ReadPixel(x,y)) >> 16) & 0xFF);
            std::stringstream body;
            body << R"(<rect width="1" height="1" x=")" << std::to_string(x) << "\" y=\"" << std::to_string(y) << "\" style=\"fill:white\"/>\r\n";

            if(value == 255) {
                std::stringstream stringStream;
                stringStream << std::hex << body.str().size() << "\r\n";
                stringStream << body.str();
                stringStream << "\r\n";

                chunk = stringStream.str();
                sendFunc(chunk);
            }
        }
    }

    MSG("Writing SVG END");
    std::string end = "</svg>\r\n";
    svgSend << std::hex << svgStart.size() << "\r\n";
    svgSend << end;
    svgSend << "\r\n";
    sendFunc(svgSend.str());

    MSG("Sending finish chunk");
    std::stringstream stringStream;
    stringStream << std::hex << 0 << "\r\n";
    stringStream << "Cache-Control: no-cache\r\n";
    stringStream << "Content-Length: 0\r\n";
    stringStream << "Connection: close\r\n";
    stringStream << "\r\n";

    chunk = stringStream.str();
    LOG("end: %s", chunk.data());
    sendFunc(chunk);
    MSG("DONE");

    GetContext()->socket->close();
    return Ok();
}
