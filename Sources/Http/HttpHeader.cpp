#include "Http/HttpHeader.h"

HttpHeader* HttpHeader::Parse(const char *data) {
    std::string request = data;
    std::string delimer = "\r\n\r\n";
    std::string header = request.substr(0, request.find(delimer));

    HttpHeader* httpHeader = new HttpHeader();

    std::istringstream headerStream(header);
    std::string line;
    std::string methods[] = {"method", "path", "version"};
    while(std::getline(headerStream, line)) {
        if(line.find(": ") == std::string::npos) {
            size_t pos = 0;
            std::stringstream method(line);
            std::string token;
            while(std::getline(method, token, ' ')) {
                httpHeader->Entries.insert(std::pair<std::string, std::string>(methods[pos], token));
                ++pos;
            }
        } else {
            std::stringstream entry(line);
            std::string token;
            std::string keyValue[2];
            size_t pos = 0;
            while(std::getline(entry, token, ':')) {
                keyValue[pos] = token;
                ++pos;
            }
            httpHeader->Entries.insert(std::pair<std::string, std::string>(keyValue[0], keyValue[1]));
        }
    }

    return httpHeader;
}

std::string HttpHeader::Method() {
    return Entries["method"];
}

std::string HttpHeader::Path() {
    return Entries["path"];
}

std::string HttpHeader::Version() {
    return Entries["version"];
}

void HttpHeader::AddEntry(const std::string &key, const std::string &value) {
    Entries.insert(std::pair<std::string, std::string>(key, value));
}

const char* HttpHeader::GetEntry(const std::string &key) {
    auto it = Entries.find(key);
    if(it != Entries.end()) {
        return it->second.data();
    } else {
        return nullptr;
    }
}

std::map<std::string, std::string>* HttpHeader::GetEntries() {
    return &Entries;
}