#include "Http/Controller.h"

Controller::Controller(const std::string &base) {
    BaseRoute = base;
}

void Controller::RegisterAction(const std::string &path, Callback<ActionResult()> actionCallback) {

    Actions.insert(
            std::pair<std::string, Callback<ActionResult()>>(path, actionCallback)
            );
}

Callback<Action()> * Controller::GetAction(const std::string &key) {
    auto it = Actions.find(key);
    if(it != Actions.end()){
        return &(it->second);
    } else {
        return nullptr;
    }
}

const char* Controller::GetBaseRoute() {
    return BaseRoute.data();
}

ActionResult Controller::Ok() {
    return ActionResult {
        Code::OK
    };
}

ActionResult Controller::BadRequest() {
    return ActionResult {
        Code::BadRequest
    };
}

ActionResult Controller::NotFound() {
    return ActionResult {
        Code::NotFound
    };
}

ActionResult Controller::UnAuthorized(){
    return ActionResult {
        Code::Unauthorized
    };
}

HttpContext* Controller::GetContext() {
    return Context;
}

void Controller::SetContext(HttpContext *context) {
    Context = context;
}
