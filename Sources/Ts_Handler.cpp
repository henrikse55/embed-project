#include "Ts_Handler.h"
#include "keyboard.h"

//*******************************************************/
// Constructors and Desstructors
//*******************************************************/
Ts_Handler::Ts_Handler(LCD_DISCO_F746NG& lcd) {
    this->lcd = lcd;
    this->State = this->ts.Init(lcd.GetXSize(), lcd.GetYSize());
    lcd.DisplayStringAt(0, LINE(5), (uint8_t *)"Initializing Touch", CENTER_MODE);
#ifdef DEBUG
    logger->Log("Initializing Touch");
#endif

    if (this->InitSucess() != TS_OK) {
        lcd.Clear(LCD_COLOR_RED);
        lcd.SetBackColor(LCD_COLOR_RED);
        lcd.SetTextColor(LCD_COLOR_WHITE);
        lcd.DisplayStringAt(0, LINE(5), (uint8_t *)"TOUCHSCREEN INIT FAIL", CENTER_MODE);
        logger->Log("Touchscreen init Failed");
    } else {
        lcd.Clear(LCD_COLOR_GREEN);
        lcd.SetBackColor(LCD_COLOR_GREEN);
        lcd.SetTextColor(LCD_COLOR_WHITE);
        lcd.DisplayStringAt(0, LINE(5), (uint8_t *)"TOUCHSCREEN INIT OK", CENTER_MODE);
        logger->Log("Touchscreen init OK");
    }

    MSG("Setting colors");
    lcd.SetFont(&Font12);
    lcd.SetBackColor(LCD_COLOR_BLACK);
    lcd.SetTextColor(LCD_COLOR_WHITE);

    MSG("Finished boot");
    lcd.Clear(LCD_COLOR_BLACK);
}

Ts_Handler::~Ts_Handler() {
        
}

//*******************************************************/
// Public Member Functions
//*******************************************************/

int Ts_Handler::InitSucess() {
    return this->State;
}

int Ts_Handler::IsTouched() {
    this->ts.GetState(&this->Ts_State);
    return this->Ts_State.touchDetected;
}

TS_StateTypeDef Ts_Handler::Dispatch() {
    return this->Ts_State;
}

char* Ts_Handler::Keyboard(const char* title) {
    
    lcd.Clear(LCD_COLOR_BLACK);
    int toggle = 0, done = 0;
    std::string result;

    while(1) {
        
        //Draw current text
        this->lcd.DrawRect(20, LINE(5), this->lcd.GetXSize() - 40, 40);
        this->lcd.SetFont(&Font20);
        this->lcd.DisplayStringAt(25, 75, (uint8_t*)result.c_str(), LEFT_MODE);
        this->lcd.SetFont(&Font12);
        
        this->lcd.SetFont(&Font20);
        this->lcd.DisplayStringAt(0, LINE(1), (uint8_t*)title, CENTER_MODE);
        this->lcd.SetFont(&Font12);
        
        //Create Keyboard Grid
        unsigned int x = 0, y = 120;
        for(int layer = 0; layer < KEYLAYER; layer++) {
            for(int i = 0; i < KEYSIZE; i++) {
                Key k = dk[layer][i];
                if(k.type != NONE){
                    //                 x  y  w   h
                    this->lcd.DrawRect(x, y, 45, 30);
                    this->lcd.DisplayChar(x+20, y + 10, k.charater);
                }
                x += lcd.GetXSize() / KEYSIZE;
            }
            y += 33;
        }
        
        //Check if touched
        if(this->IsTouched()) {
            
            //Make sure it's a "clear" state
            if(!toggle && this->Ts_State.touchY[0] >= 120){
                unsigned int posY = (this->Ts_State.touchY[0] - 120) / 33;
                unsigned int posX = this->Ts_State.touchX[0] / (lcd.GetXSize() / KEYSIZE);
                
                //Skip rest of the code as I only have 4 layers
                if(posY > 3) continue;
                Key key = dk[posY][posX];
                if(key.type == NONE) continue;
                
                //Handle Key based on type
                char selected = key.charater;
                switch(key.type) {
                    case NORMAL:
                        result += selected;
                        break;
                    case SPECIAL:
                        switch(selected) {
                            case '-':
                                if(result.length() > 0)
                                    result.resize(result.length()-1);
                                break;
                            case '@':
                                done = 1;
                                break;
                        }
                        break;
                    case NONE:
                        break;
                }
                
                //Highlight of box
                this->lcd.SetTextColor(LCD_COLOR_BLUE);
                posX = (lcd.GetXSize() / KEYSIZE) * posX;
                posY = (33 * posY) + 120 + posY;
                this->lcd.FillRect(posX, posY, 45, 30);
                this->lcd.SetTextColor(LCD_COLOR_WHITE);
            }
            toggle = 1;
        } else {
            
            //Clear and stop if done
            if(toggle){
                toggle = 0;
                this->lcd.Clear(LCD_COLOR_BLACK);
                if(done) break;
            }
        }
    }
    
    //Copy string over to buffer
    result += '\0';
    char* buffer = new char[result.length()];
    sprintf(buffer, "%s", result.c_str());
    
    return buffer;
}
