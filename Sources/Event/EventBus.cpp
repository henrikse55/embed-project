#include "Event/EventBus.h"
#define TRACE

EventBus* EventBus::GetInstance() {
    static EventBus eventBus;
    return &eventBus;
}

void EventBus::RegisterEvent(const std::string &eventName, const Callback<void(message_t *)>& function) {

    logger->Log("Registered new event(%s)", eventName.c_str());

    char* value = new char[eventName.size()+1];
    memset(value, 0, sizeof(char) * eventName.size()+1);
    memcpy(value, eventName.c_str(), sizeof(char) * eventName.size());

    Handler* handler = new Handler{
        value,
        function
    };

    handlers.push_back(handler);
}

/**
 * Add event message for pass-through
 * @param event
 * @return
 */
bool EventBus::Add(message_t* event) {
    if(!queue.full()){
        queue.put(event);
#ifdef TRACE
//        logger->Log("Adding to queue %d", status);
#endif
    } else {
        logger->Log(Level::WARNING,"Event queue is full");
        return true;
    }
    return false;
}

void EventBus::Start() {
    Run = true;
    logger->Log("EventBus is now running");
    while(Run) {
        osEvent event = queue.get();
        message_t* message = (message_t*)event.value.p;
        for(Handler* handler : handlers) {
#ifdef TRACE
            logger->Log("Handler: %s | event %s | cmp: %d | function: %p",
                    handler->event,
                    message->event,
                    std::strcmp(handler->event, message->event),
                    handler->func);
#endif
            if(std::strcmp(handler->event, message->event) == 0) {
#ifdef TRACE
                logger->Log(Level::WARNING, "Event Start %s, func: %p",handler->event, &handler->func);
#endif
                handler->func(message);
#ifdef TRACE
                logger->Log(Level::WARNING, "Event Stop %s, func: %p", handler->event, &handler->func);
#endif
            }
        }
        pool.free(message);
    }
}

message_t* EventBus::CreateMessage() {
    return pool.alloc();
}

message_t* EventBus::CreateMessage(const char* event) {
    message_t* msg = pool.alloc();
    msg->event = event;
    return msg;
}