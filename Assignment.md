# HF5 Embedded controller assigment

## Project Information

|              |   | 
|--------------:|:---|
| Project Name |  HF4 refactor |
| Creator      | Henrik Malmer Lassen  |
| Date         |  19/8/2019 |

#### Libraries
| Name | Version | Short Description |
|:------:|:---------:|:-------------------:|
|  MBed OS<sup>1</sup>  | 5.10 | Main controller "OS" library |
|  LCD Disco  | 2  | Main LCD abstraction library |
|  BSP Disco |  2  | LCD util & driver lib |
|  Grove Disco<sup>2</sup> | ? | Grove display abstraction lib  |


1. Newer versions are present at the time of writing (~5.13) but seems to break the video library I'm using.
2. Grove display is currently not in use

## General overview
The following information is to give an effective birds eye view of some of major changes.

### Folder Structure\*

Root: /Masters

| Path | Short Description |
|:----:|:-----------------:|
| Constants/ | Contains const values getting repeated in many files |
| Headers/Event/ | Contains event related header files |
| Headers/Http/ | Contains everything speceficly related to http implementation |
| Headers/Network | Contains networking specefic content |
| Headers/ | Uncatigorized files |

\* This structure carries over to implementation side.

ex: Sources/Event/EventBus -> Sources/Event/EventBus

## General Case
From the a former project, I wanted to update, improve and restructure my old HF4 source code with higher focus of the web server.


## Custom Functionality

## Smarter Serial based logging

In the original code, every kind of logging was done via a simple printf(string,...).
This caused visual issues when "logging" from multiple threads at one.
while one thread could be in process of writing anoher thread could at any given moment also start doing so.

To fix this issue a queue have been implemented, this queue uses the producer -> consumer pattern with that every
thread can enqueue an message to be logged.

The loggers consumer thread is then responsible of printing the actual content out to serial.
This is also the only thread which will/should access the serial interface at all.

#####  Logger adds minor debugging problems

This logging have functioned really well while working on this, the only bad thing here is the fact logging happens
on another thread than what orignally made the entry.

Such have given wrong thread names and stack dumps during system hard faults making it a little harder to debug but
by reading the actual log message it's easy to find the correct location of the error occurrence and it's not uncommon
for a the fault to happen before the entry have been completely written.

##### Color codes
Quickly after I got the general logger functionality, I wanted to change the color of each line based on what happens
This would serve as a little eye candy as well as highlighting more interesting/rare events

##### Logger mutex vs queue and consumer thread

In the beggening the logger was a static object that all threads accessed via a pointer and used a mutex to prevent
multiple threads from writing at once.

using the mutex guaranteed every entry would be written before the error occurred but with every thread using 
the same instance, made the whole application run like a single threaded application because every thread needed
to wait for the lock.

I decided it would after reaching such a high demand on the logger to lose a little information in 
the stack dump during a hard fault simply to retain multi thread functionality.

![The current result of how the logger looks](./Images/Serial-Logger.png)

## Event bus

With modularity and code separation in mind, I wanted to create a event driven system which
didn't rely on object pointers because it can pose instance difficulties and potentially _Hard dependencies_<sup>1</sup>

##### Calling class functions based on pointers

I avoided using function pointers a couple of reasons

1. Non-static object instances aren't guarantied to be in the same location always
2. The Scope is constantly chancing
3. I wanted to avoid _Hard dependencies_ and force a class upon one

##### The scope problem

To touch a little up on point 2 and 3 about scopes and dependencies I wanted to give a smaller example.

looking at my HTTP setup, I have two controllers implemented: HomeController and ImageController

If I wanted to use a raw function pointer with those two I could because they both extends the Controller class.

It's scope would look like
```cpp
    void (Controller::*)(message_t* message);
```

This does however throw a couple of disadvantages at me.

If I did this everything would need to extend Controller and the controller would either have to define
one common "on event" function or many virtual ones.

this could be avoided in the Controller by making a dedicated class for event receiving, 
but then every single class or code which wanted to be a part of the event bus would need to extend such class.

I dedcided to use `mbed::Callback<...>` as that allows me to use simple function pointer definitions like
`void (*)(message_t*)` and still be able to invoke in specefic object instances but by calling `someCallback(message);`,
then the mbed lib will take of the rest.

##### Name based events

For triggering an event I didn't wanna give pointer or int's (i've tried that and it's not fun to remember), So I decided
to use const char*, char* because I don't need the functionality strings gives me comparing event names.

`strcmp()` Does take const char* so it seemed clear what to use, this probably also reduced overhead as the event bus is something 
which is getting called multiple times.

<sup>1. Hard dependencies: A peace of code or function which can't function without some "external" code outside the function </sup>

## HTTP Server

### Separated TCP/IP communication

Even though the plan was to only present a HTTP server through the controller I wanted to separate the socket code
from the actual HTTP handling, during this I utilized my new event driven bus by
having a thread constantly wait for network communication
and then notify the HTTP syb-system through the bus.

This allows great code separation and relative simple to open new systems without having to copy a lot of the code from the HTTP server
because it's not responsible for socket management.

### HTTP Abstractions

To Reduce the amount of manual string manipulation and make it clearly separate I created a couple of different classes
that would function as "HTTP to C++" proxy abstractions.

I currently have have 5 Abstraction "Levels"

| Name | Function |
|:---:|:---|
| Controller Dispatcher | Retries TCP data from the event bus, locates and executes the request content|
| HttpContext | parses and manages the request, response and body |
| HttpResponse | Contains HTTP response data |
| HttpRequest | Contains HTTP request data|
| Controller<sup>1</sup>| Base implementation of a resource|

<sup>1. The controller class in itself dosen't handle HTTP requests, it's more a collection of functions 
that will be used across all controllers extending it</sup>

### Controllers

I like MVC structures, it seems well organised, therefor I took inspiration form such structure with this implementation.

Having controllers write the actual HTTP content seperate from the socket handling always feels nice and _reduces_ 
the amount of code needed to read/know about at once.
With how the data also slows with this makes it generally simpler to understand also.

With how I've implemented the controllers I also have a simple yet effective method of control what/where and when an
 _Action_<sup>1</sup> is available

<sup>1. Action: A function in a controller which can be called by accessing a certain url</sup>

#### Security

Due to knowledge, time and library limitations I didn't have time to implement TLS 1.3 support on the sockets, so all 
communication is currently going over the internet in blank text, which aren't ideal.

##### Mbed TLSSocket client only?

Mbed does in the later >=5.10 versions have a TLSSocket class available but due to limited documentation and description
I've been unable to make TLS work 100%, though I was able to make the browser complain about invalid SSL protocol.

All the doucmentation shows how to run the socket as a client and not server, it's even so far that when trying to listen
and accept it throw a not implemented error.

###### Basic Authentication 

I did want some level of security, even though the content is sent through the internet in clear text, I wanted to limit
who could access it at least.

one could argue as it's still blank text and the username and password is sent in blank text, this doesn't do much but it's something :)

###### Base64 Encoded
Base64 is used for basic auth in the HTTP header containing:
```http
Authorization: basic [base64-encoded-payload]
```

This posed a minor challange for to me because c/c++ don't seem to have base64 encode/decode functionality, mbed did 
have some tls related base64 functions but I didn't trust those so I found one online and converted them into the encoding class

###### The window

After I found and validated the base64 functions, I took went into the [Home Controller](./Sources/Http/Controller/HomeController.cpp)
and implemented a the basic auth login window.

This login system is an effective and easy way to limit who has access to specefic people and prevent indexing services
from indexing testing servers or like.

![How it looks when you try to access the location without being authenticated](./Images/Basic-Auth.png)

#### HTTP Chunk Encoding

Due to the limitations of the controller with how much memory is present (360Kb), this isn't enough to send more complex responses.

To come around this issue, I implemented HTTP chunk encoding which allows you to send data in peaces.
how this works is quite simple, when you send content you will always include the contents size in bytes encoded in hex
in the first line, content and then a CLRF line at the end

```
5\r\n
hello\r\n
\r\n
```

To inform the client that you are done sending you will send an empty chunk.
```
0\r\n
\r\n
```

Though you *can* send additional headers in this end chunk it doesn't effect the chunk size.
##### SVG image

with chunk encoding working it became possible for me to send the contents of the controllers screen.
This wouldn't have been possible without some form of chunking either in HTTP or how I used the SD card.

I could have done something like writing the data to a SD card and then read peaces of it over time, if correct `Content-Length`
was given the client would just wait for me to send it all.
this would however have increased the complexity greatly and add more places where the code can reach unexpected issues due to being an external source

###### SVG Performance

The first version wasn't that performance, over the span of 1.5 minutes managed to send **every single pixel** with a total
download size of 9.2 MB

An optimization which was done to reduce is only send what's needed, by setting the SVG background-color to the default background
color of the display (black) I could drastically decrease the data needed to be sent to 380 KB (75x smaller than original size)
and reduced the time to ~30 seconds

![What you will see when opening the controller action](./Images/Svg-Image.png)

#### Bitmap

Due to complaints about how I misused SVG, I ended up making almost the same implementation of the SVG image but sending a bitmap
instead of SVG rects with x, y positions.

This was a nice challenge because i didn't have much experience in writing such structures in c++ before, but after a short while
I got something that loads much faster than the SVG image because I don't need to form it as html and can just send raw bytes.

Apart from the weird line at the bottom it almost also looks identical to the SVG images, apart from scaling much better.

![Bitmap example](./Images/bitmap-example.bmp)

# Working/Compiling Locally

## Downloading
Firstly I Downloaded my project down to my local machine using the online compilers export function to export as Cmake-GCC-ARM

![Export window](./Images/Online-Download-Cmake.png)

## Modifying the CMake file
with how you get the project, it's basicly setup but I ended modifying it because the compiler paths didn't match with where they where located on my machine

You may not need to change these if you added the compilers to your PATH enticement

![Forcing compilers](./Images/Compiler-locations.png)

I would absolute paths to work best

##### Custom Command Fixes

After changing the above, I could almost compile successfully, mbed have just added a custom command which didn't use full path,
making this pointer to the absolute path of the gcc compiler made it successful at compiling.

![this command fails](./Images/Add-Custom-Command.png)

Afterwards I was a little lost on how I could run it on the controller as it didn't compile any .bin files.
it does make an .out file so adding the command bellow (with respect to the project names will differ)

![The command I've added to make a bin file](./Images/Compiled-to-bin.png)