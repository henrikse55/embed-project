#ifndef MASTERS_HOMECONTROLLER_H
#define MASTERS_HOMECONTROLLER_H

#include "Http/Controller.h"

class HomeController : public Controller {
public:
    explicit HomeController(const std::string& base);
    ActionResult Index();
    ActionResult Login();
};

#endif //MASTERS_HOMECONTROLLER_H
