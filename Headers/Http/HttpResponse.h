#ifndef MASTERS_HTTPRESPONSE_H
#define MASTERS_HTTPRESPONSE_H

#include "Event/EventBus.h"
#include "Logger.h"
#include "Http/HttpHeader.h"
#include "pch.h"

class HttpResponse{
public:
    HttpHeader Header;
    std::stringstream body;
};

#endif //MASTERS_HTTPRESPONSE_H
