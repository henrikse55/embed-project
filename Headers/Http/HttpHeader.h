#ifndef MASTERS_HTTPHEADER_H
#define MASTERS_HTTPHEADER_H

#include "Event/EventBus.h"
#include "Logger.h"
#include "pch.h"
#include <map>
#include <sstream>
#include <algorithm>
#include <iterator>

class HttpHeader {
public:
    static HttpHeader* Parse(const char* data);

    std::string Path();
    std::string Version();
    std::string Method();

    void AddEntry(const std::string& key, const std::string& value);
    const char* GetEntry(const std::string& key);
    std::map<std::string, std::string>* GetEntries();

private:
    std::map<std::string, std::string> Entries;

    ENABLE_LOGGER
    ENABLE_BUS
};

#endif //MASTERS_HTTPHEADER_H
