#ifndef MASTERS_CONTROLLERDISPATCHER_H
#define MASTERS_CONTROLLERDISPATCHER_H

#include "pch.h"
#include "HttpContext.h"
#include "Controller.h"
#include "Network/ServerSocket.h"
#include "vector"

class ControllerDispatcher {
public:
    ControllerDispatcher();

    void Notify(message_t* message);
private:
    std::vector<Controller*> Controllers;

    ENABLE_LOGGER
    ENABLE_BUS

    void RegisterNotify(message_t *message);
    Mutex mutex;
};

#endif //MASTERS_CONTROLLERDISPATCHER_H
