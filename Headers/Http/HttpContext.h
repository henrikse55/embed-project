#ifndef MASTERS_HTTPCONTEXT_H
#define MASTERS_HTTPCONTEXT_H

#include "pch.h"
#include "HttpRequest.h"
#include "HttpResponse.h"

class HttpContext {
public:
    HttpRequest* request;
    HttpResponse* response;

    TCPSocket* socket;
    bool DontSend = false;
    std::string FormResult(const ActionResult& result);

private:
    ENABLE_LOGGER
    ENABLE_BUS
};

#endif //MASTERS_HTTPCONTEXT_H
