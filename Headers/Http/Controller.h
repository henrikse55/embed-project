#ifndef MASTERS_CONTROLLER_H
#define MASTERS_CONTROLLER_H

#include "pch.h"
#include "map"
#include "HttpContext.h"

class Controller {
public:
    HttpContext* GetContext();
    void SetContext(HttpContext* context);

    Callback<Action()> *GetAction(const string &key);

    const char* GetBaseRoute();
protected:
    explicit Controller(const std::string& base);

    ActionResult BadRequest();
    ActionResult Ok();
    ActionResult NotFound();
    ActionResult UnAuthorized();

    void RegisterAction(const std::string& path, Callback<ActionResult()> actionCallback);

    ENABLE_LOGGER
    ENABLE_BUS
private:
    std::string BaseRoute;
    std::map<std::string, Callback<ActionResult()>> Actions;
    HttpContext* Context;

};

#endif //MASTERS_CONTROLLER_H
