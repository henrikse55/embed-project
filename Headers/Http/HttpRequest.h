#ifndef MASTERS_HTTPREQUEST_H
#define MASTERS_HTTPREQUEST_H

#include "Event/EventBus.h"
#include "Logger.h"
#include "Http/HttpHeader.h"
#include "pch.h"

class HttpRequest {
public:
    HttpHeader* Header;
    std::map<std::string, std::string> Queries;
};

#endif //MASTERS_HTTPREQUEST_H
