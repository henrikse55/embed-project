#ifndef MASTERS_RESPONSECODES_H
#define MASTERS_RESPONSECODES_H

#define TOSTRING(name) #name

//TODO: I'm not sure how to feel about this implementaiton, could this be done in a better way?
enum Code {
    OK  = 200,

    MovedPermanently = 301,
    MovedTemporarily = 302,

    BadRequest = 400,
    Unauthorized = 401,
    Forbidden = 403,
    NotFound = 404,
};

struct Response {
    Code code;
};

inline
const char* CodeToString(Code code) {
    switch(code){
        case OK: return "200 OK";
        case NotFound: return "404 NotFound";
        case Forbidden: return "403 Forbidden";
        case BadRequest: return "400 Bad Request";
        case MovedPermanently: return "301 Moved Permanently";
        case MovedTemporarily: return "302 Moved Temporarily";
        case Unauthorized: return "401 Unauthorized";
    }
    return nullptr;
}

#endif //MASTERS_RESPONSECODES_H
