#ifndef MASTERS_ETHERNET_H
#define MASTERS_ETHERNET_H

#include "EthernetInterface.h"
#include "Logger.h"
#include "Event/EventBus.h"
#include "Thread.h"
#include "LoggerEvents.h"


class Ethernet {
public:
    void Initialize();
    ~Ethernet();

    static Ethernet *GetInstance();

    NetworkInterface *GetInterface();
private:
    bool Connected = false;
    Thread StateWatch;
    NetworkInterface* interface;
    Logger* logger = Logger::GetInstance();
    EventBus* bus = EventBus::GetInstance();

    void StateChange();
};

#endif //MASTERS_ETHERNET_H
