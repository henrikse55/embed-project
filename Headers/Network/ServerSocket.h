#ifndef MASTERS_SERVERSOCKET_H
#define MASTERS_SERVERSOCKET_H

#include "Logger.h"
#include "Event/EventBus.h"
#include "pch.h"

struct Connector {
    char* message;
    TCPSocket* socket;
    bool Pending = false;
};

//TODO Is this struct actually needed? probably not
struct Connection {
    char* message;
    TCPSocket* socket;
};

class ServerSocket {
public:
    ServerSocket(size_t port, const std::string& event);
//    ~ServerSocket();

    void Notify(message_t* message);

private:
    void Listen();

    ENABLE_LOGGER
    ENABLE_BUS

    volatile bool running = false;
    size_t port;
    TCPSocket server;
    Thread* thread;
    std::string event;
    NetworkInterface* interface;
    std::vector<Connector*> connections;

    void RespondToSocket(message_t* message);
};

#endif //MASTERS_SERVERSOCKET_H
