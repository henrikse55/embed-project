#include "PlatformMutex.h"
#include <string>
#include <iostream>
#include "mbed.h"

#ifndef MASTERS_LOGGER_H
#define MASTERS_LOGGER_H

#define ENABLE_LOGGER Logger* logger = Logger::GetInstance();
#define MSG(msg) logger->Log(msg)
#define LOG(msg, ...) logger->Log(msg, __VA_ARGS__)
#define CLOG(level, msg, ...) logger->Log(level, msg, __VA_ARGS__)

enum Level {
    INFO,
    NOTICE,
    WARNING,
    FATAL
};

struct Data{
    char* data;
    ~Data(){
        delete data;
    }
};

class Logger {
public:
    Logger() {
        pc = new Serial(USBTX,USBRX,115200);
        pc->enable_input(false);
        pc->format(8, SerialBase::Even, 1);

        pc->putc(27);
        printf("[2J");

        thread.start(callback(this, &Logger::Dispatch));
    }
    template<typename ...values>
    void Log(const char* message, values... args)
    {
        Log(defaultLevel, message, args...);
    }

    inline
    void Log(Level level, const char* message, ...)
    {
        const char* lvl = getLevelString(level);
        char combined[256];
        memset(combined, 0, sizeof(char) * 256);
        va_list args;
        va_start(args, message);
        vsprintf(combined, message, args);
        va_end(args);
        char* entryText = (char*)malloc(sizeof(char) * 512); //TODO: Overcomplicate size allocation for minimal size
        memset(entryText, 0, sizeof(char) * 512);

        const char* color = GetColor(level);

        sprintf(entryText, "%llo [%s] %s%s\033[0m", time(nullptr), lvl, color, combined);
        Data* data = new Data {
            entryText
        };
        queue.put(data);
    }

    inline
    const char* GetColor(Level level)
    {
        switch(level) {
            case NOTICE:  return "\033[22;33m";
            case WARNING: return "\033[1;33m";
            case FATAL:   return "\033[1;31m";
            case INFO:    return "\033[0m";
            default:      return "\033[0m";
        }
    }

    void Dispatch() {
        this->Log(Level::NOTICE, "Logger writer thread have started");
        while(true) {
            osEvent event = queue.get();
            Data* data = (Data*)event.value.p;
            //TODO: Detect LARGE writes as it will crash the controller, make large writes have appended start space
            //Validate is new lines are present or cut at set interval if not available
            printf("%s\r\n", data->data);
            delete data;
        }
    }

    inline
    static Logger* GetInstance() {
        static Logger logger;
        return &logger;
    }

private:
    static const char* getLevelString(Level level) {
        switch(level) {
            case Level::INFO:
                return "\033[0mINFO";
            case Level::NOTICE:
                return "\033[22;33mNOTICE\033[0m";
            case Level::WARNING:
                return "\033[1;33mWARNING\033[0m";
            case Level::FATAL:
                return "\033[1;31mFATAL\033[0m";
        }
        return "";
    }
    Mutex lock;
    Thread thread;
    Level defaultLevel = Level::INFO;
    Serial* pc;
    Queue<Data, 512> queue;
};


#endif //MASTERS_LOGGER_H
