#ifndef MASTERS_ENCODING_H
#define MASTERS_ENCODING_H

#include <iostream>
#include <string>

class Encoding {
public:
    std::string Encode(const unsigned char* bytes_to_encode, unsigned int in_len);
    std::string Decode(std::string const& encoded_string);

    static inline bool is_base64(unsigned char c) {
        return (isalnum(c) || (c == '+') || (c == '/'));
    }
private:
    const std::string base64_chars =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            "abcdefghijklmnopqrstuvwxyz"
            "0123456789+/";
};


#endif //MASTERS_ENCODING_H
