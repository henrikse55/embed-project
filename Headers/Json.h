#ifndef _WEB_JSON_
#define _WEB_JSON_

#include "pch.h"

//TODO Improve upon this json system
struct JsonValue {
    char* name;
    char* value;    
};

class Json {
public:
    Json(JsonValue* items, unsigned int count);
    ~Json();
    char* GetJson();
    
private:
    size_t GetBufferSize();
    unsigned int size;
    JsonValue* items;
};

#endif