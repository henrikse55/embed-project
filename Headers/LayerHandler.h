#ifndef _LAYER_HANDLER_
#define _LAYER_HANDLER_

#include "pch.h"
#include <vector>
#include "Logger.h"
#include "Event/EventBus.h"

struct Render {
    size_t x, y, width, height;
    char* title;
    char* content;
    bool visible;
    
    void (*handle)(const size_t id);
};

struct Layer {
    Render* renders;
    size_t size;
};

//TODO: Convert to use event bus
class LayerHandler {
public:
    LayerHandler(LCD_DISCO_F746NG& lcd);
    ~LayerHandler();

    void ChangeLayer(const size_t layer);
    size_t AddLayer(Render* layer, size_t size);
    
    void Draw();
    void CallPressed(const TS_StateTypeDef touch);
    
    void SetVisibleByTitle(const char* title, bool value);
    void SetVisibleByIndex(size_t index, bool value);
    
    bool UpdateContent(const size_t index,const char* content);
    
    Render* FindRenderByPos(const uint16_t x, const uint16_t y);
    void MoveToPos(Render* render, const size_t x, const size_t y);
    
    void DrawGraphAt(float* values, const size_t size, const size_t x, const size_t y, const size_t area);

    void Notify(message_t* message);
private:
    void RemoveOldText(const size_t x, const size_t y, const size_t width, const size_t height);
    bool IsEqual(const char* first, const char* second);
    
    void DrawRender(Render* render);
    
    LCD_DISCO_F746NG lcd;
    std::vector<Layer> layers;
    size_t current;
    Logger* logger = Logger::GetInstance();
    Mutex mutex;

    ENABLE_BUS
};

#endif