#ifndef _LIMITEDLINKEDLIST_
#define _LIMITEDLINKEDLIST_

///
// Structure Template
///
template<typename T>
struct Item
{
    T value;
    Item* next;
};

///
// Class Definition Template
///
template<typename T, typename TSize>
class LimitedLinkedList
{
public:
    LimitedLinkedList(const TSize size) {
        this->Current = 0;
        this->Size = size;
    }

    //Get the current count of items in the list
    //Please be carefull, this is an expensive task!
    //Should probably store the current count somewhere
    TSize Count() {
        TSize index = 0;
        Item<T>* cur = this->Current;
        if (cur != 0) {
            while ((cur = cur->next) != 0) ++index;
        }
        return index+1;
    }

    //Get all linked items values and return an array of the value type
    T* GetAllValues() {
        const TSize count = this->Count();
        T* values = new T[count];
        for (TSize index = 0; index < count; index++)
            values[index] = ((Item<T>*)this->operator[](index))->value;

        return values;
    }

    //Push the new value into the list on top
    void push_back(const T value) {
        Item<T>* item = new Item<T>();
        item->value = value;

        if (this->Current != 0) {
            Item<T>* cur = this->Current;
            item->next = cur;

            this->Current = item;
            TSize count = this->Count();
            
            //Remove and clean the end if limit is reached
            if (count >= this->Size) {
                Item<T>* last = ((Item<T>*)this->operator[](count-1));
                delete last;
                ((Item<T>*)this->operator[](count - 2))->next = 0;
            }
        } else {
            this->Current = item;
        }
    }

    //Change the size limit doing runtime
    void SetSize(TSize size) {
        this->Size = size;
    }
    
    //Get a struct item pointer based on index
    Item<T>* operator[](TSize index) {
        Item<T>* cur = this->Current;
        TSize idx = 0;
        while (idx != index)
        {
            if (cur->next != 0) {
                cur = cur->next;
            } else {
                return 0;
            }
            ++idx;
        }
        return cur;
    }

private:
    TSize Size;
    Item<T>* Current;
};

#endif