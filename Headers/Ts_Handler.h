#ifndef _TS_HANDLER_
#define _TS_HANDLER_

#include "pch.h"
#include "Logger.h"
#include "Event/EventBus.h"

class Ts_Handler{
public:
    Ts_Handler(LCD_DISCO_F746NG& lcd);
    ~Ts_Handler();

    int InitSucess();
    int IsTouched();
    
    TS_StateTypeDef Dispatch();
    
    char* Keyboard(const char* title);
    
private:
    size_t limit;
    uint8_t State;
        
    TS_StateTypeDef Ts_State;
    TS_DISCO_F746NG ts;
    LCD_DISCO_F746NG lcd;
    Logger* logger = Logger::GetInstance();
    EventBus* eventBus = EventBus::GetInstance();
};

#endif