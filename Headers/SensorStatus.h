#ifndef MASTERS_SENSORSTATUS_H
#define MASTERS_SENSORSTATUS_H

#include "pch.h"

class SensorStatus {
public:
    SensorStatus(LayerHandler* layer, LCD_DISCO_F746NG* lcd);

    void Update();
    void ToggleUpdate(message_t* message);

private:
    float GetTemp();
    float GetSound();
    float GetLight();

    Thread thread;

    volatile bool VideoToggle = false;

    LimitedLinkedList<float, short int>* TempValues;
    LimitedLinkedList<float, short int>* SoundValues;
    LimitedLinkedList<float, short int>* LightValues;

    LayerHandler* video;
    LCD_DISCO_F746NG* lcd;

    ENABLE_BUS
    ENABLE_LOGGER

};


#endif //MASTERS_SENSORSTATUS_H
