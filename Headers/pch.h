#ifndef _PCH_
#define _PCH_

/* std Library Inlcudes*/
#include <string>
#include <iostream>
#include <sstream>
#include <istream>

/* Mbed OS */
#include "mbed.h"
#include "LCD_DISCO_F746NG.h"
#include "TS_DISCO_F746NG.h"
#include "Thread.h"
#include "PlatformMutex.h"


/* My Headers */
//TODO Make data updaters for sensors and incorporate it into the bus system
#include "Json.h"
#include "LimitedLinkedList.h"
#include "LayerHandler.h"
#include "Ts_Handler.h"
#include "Http/ResponseCodes.h"
#include "General.h"
#include "LoggerEvents.h"

/* Useful redefine macros */
#define Mutex PlatformMutex

/* Constants */
#define DEBUG
#define TRACE
#define LIGHT_SENSOR A1
#define SOUND_SENSOR A2
#define TEMP_SENSOR A3
#define STATE_BUTTON D4
#define TMP_HIGH 125

#define RGB_LCD_LINE 17

/*Macros*/

#define VoltageToC(x) (float)((float)x - 0.5F) * 100
#define CToF(c) (float)((float)c * 1.8F) + 32
#define prog(current) (current / TMP_HIGH) * (RGB_LCD_LINE * 2)

#define UpdateData(i, x) video->UpdateContent(i, x); delete x

typedef struct Action {
    Code code;
    //TODO: Additional data for body, headers, etc?
} ActionResult;

/* Utility Functions */
inline
char* GetChar(const char* text, const float value) {
    char* buffer = new char[10];
    sprintf(buffer, text, value);
    return buffer;
}

inline
void GetStats(float* items, const size_t size, float* avg, float* max, float* min) {
    for(size_t i = 0; i < size; i++) {
        *avg += items[i];
        if(items[i] > *max) *max = items[i];
        if(items[i] < *min) *min = items[i];
    }
    
    *avg /= size;
}

#endif