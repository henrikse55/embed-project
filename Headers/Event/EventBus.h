#ifndef MASTERS_EVENTBUS_H
#define MASTERS_EVENTBUS_H

#include <vector>
#include "Logger.h"

#define ENABLE_BUS EventBus* bus = EventBus::GetInstance();

struct message_t {
    const char* event;
    size_t size;
    void* param;
};

struct Handler {
    const char* event;
    Callback<void(message_t*)> func;
};

class EventBus {
public:
    static EventBus* GetInstance();

    void RegisterEvent(const std::string& eventName, const Callback<void(message_t*)>& function);

    bool Add(message_t* event);
    message_t* CreateMessage();
    message_t* CreateMessage(const char *event);

    void Start();

private:
    Queue<message_t, 128> queue;
    MemoryPool<message_t, 128> pool;
    std::vector<Handler*> handlers;
    Mutex mutex;
    Logger* logger = Logger::GetInstance();
    bool Run = true;
};
#endif //MASTERS_EVENTBUS_H
