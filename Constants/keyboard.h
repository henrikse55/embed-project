#ifndef _KEYBOARD_
#define _KEYBOARD_

#define KEYLAYER 4
#define KEYSIZE 10

enum KeyType{
    NORMAL,
    SPECIAL,
    NONE
};

struct Key {
    KeyType type;
    char charater;
};

const Key dk[KEYLAYER][KEYSIZE] = {
    {{NORMAL, '1'}, {NORMAL, '2'}, {NORMAL, '3'}, {NORMAL, '4'}, {NORMAL, '5'}, {NORMAL, '6'}, {NORMAL, '7'}, {NORMAL, '8'}, {NORMAL, '9'}, {NORMAL, '0'}},
    {{NORMAL, 'q'}, {NORMAL, 'w'}, {NORMAL, 'e'}, {NORMAL, 'r'}, {NORMAL, 't'}, {NORMAL, 'y'}, {NORMAL, 'u'}, {NORMAL, 'i'}, {NORMAL, 'o'}, {NORMAL, 'p'}},
    {{NORMAL, 'a'}, {NORMAL, 's'}, {NORMAL, 'd'}, {NORMAL, 'f'}, {NORMAL, 'g'}, {NORMAL, 'h'}, {NORMAL, 'j'}, {NORMAL, 'k'}, {NORMAL, 'l'}, {SPECIAL, '-'}},
    {{NORMAL, 'z'}, {NORMAL, 'x'}, {NORMAL, 'c'}, {NORMAL, 'v'}, {NORMAL, 'b'}, {NORMAL, 'n'}, {NORMAL, 'm'}, {NORMAL, ' '}, {NONE, ' '}, {SPECIAL, '@'}}
};

#endif