#ifndef MASTERS_LOGGEREVENTS_H
#define MASTERS_LOGGEREVENTS_H

#define VIDEO_UPDATE "video-event"
#define VIDEO_TOGGLE "video-toggle"

#define HTTP_PARSER "http-parser"
#define HTTP_PARSER_RESPONSE "http-parser-response"

#define CONTROLLER_REGISTER "controller-register"

#define NETWORK_IP "network-ip"
#define NETWORK_CHANGE "network-change"

#endif //MASTERS_LOGGEREVENTS_H
