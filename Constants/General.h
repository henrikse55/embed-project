#ifndef MASTERS_GENERAL_H
#define MASTERS_GENERAL_H

/* Structs */
typedef struct Video {
    size_t layer = 0;
    size_t id = -1;
    const char* data = nullptr;

    ~Video(){
        delete data;
    }
} video_t;

#endif //MASTERS_GENERAL_H
