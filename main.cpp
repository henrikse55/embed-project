#include "pch.h"
#include "Logger.h"
#include "Event/EventBus.h"
#include "Network/Ethernet.h"
#include "Network/ServerSocket.h"
#include "Http/ControllerDispatcher.h"
#include "Encoding.h"
#include "SensorStatus.h"

Logger* logger = Logger::GetInstance();
EventBus* eventBus = EventBus::GetInstance();

Ethernet eth;
LCD_DISCO_F746NG lcd;
LayerHandler video(lcd);
Ts_Handler tsh(lcd);
SensorStatus status(&video, &lcd);
ServerSocket serverSocket(80, HTTP_PARSER);
ControllerDispatcher dispatcher;

#include "Http/Controller/ControllerRegistar.h" // Make sure controllers are included after dispatcher

//Check if a touch even has occoured,
//TODO, Find a way to merge this into either TS_handler or Layerhandler
bool toggle = false;
size_t doMove = 0;
void Touch() {
    if(tsh.IsTouched()) {
        TS_StateTypeDef t = tsh.Dispatch();
        video.SetVisibleByIndex(1, toggle);
        //high enough preasure should increment the "timer"
        if(t.touchWeight[0] > 45) {
            //Prepare for a render move
            if(doMove >= 50) {
                //Find the render object which to move
                Render* render = video.FindRenderByPos(t.touchX[0], t.touchY[0]);
                if(render != 0) {
                    //Move it periodicly but not constantly to recude flicker
                    int limiter = 0;
                    while(tsh.IsTouched()) {
                        t = tsh.Dispatch();
                        if(limiter >= 0) {
                            int x = t.touchX[0] - (render->width / 2);
                            int y = t.touchY[0] - (render->height / 2);

                            //Prevent the render from going out screen
                            if(x <= 0) x = 5;
                            if(y <= 5) y = 10;
                            if(x + (render->width) >= lcd.GetXSize()) x = lcd.GetXSize() - render->width - 2;
                            if(y + (render->height) >= lcd.GetYSize()) y = lcd.GetYSize() - render->height - 2;


                            video.MoveToPos(render, x, y);
                            limiter = 0;
                        }
                        ++limiter;
                    }
                    video.Draw();
                }
            }
            ++doMove;
            toggle = true;
        }else if(!toggle) {
                video.CallPressed(t);
                toggle = true;
        }
    }else{
        //Reset
        if(toggle) {
            doMove = 0;
            toggle = false;
            video.SetVisibleByIndex(1, toggle);
        }
    }
}

//Callback for building and room codes
void RenderCodes(const size_t id) {
    char* code = 0;
    if(id == 4) {
        code = tsh.Keyboard("Building Code");
    } else {
        code = tsh.Keyboard("Room Code");
    }
    
    video.UpdateContent(id, code);
    video.Draw();
}

void netip(message_t* message) {
    message_t* msg = eventBus->CreateMessage(VIDEO_UPDATE);
    video_t* video = new video_t();
    video->id = 6;
    video->data = (char*)message->param;
    msg->param = video;
    eventBus->Add(msg);
}

int main() {
    MSG("Starting bus thread");
    Thread bus(osPriority_t::osPriorityNormal, OS_STACK_SIZE, NULL, "Event bus");
    bus.start(callback(eventBus, &EventBus::Start));
    MSG("Finished");

    eventBus->RegisterEvent("network-ip", callback(&netip));

#ifdef DEBUG
    logger->Log("Initializing Render Structs");
#endif

    //Initialize Render structs
    struct Render Temperature = {5, 5, 80, 40, (char*)"Temperature", 0, true, 0};
    struct Render SoundSensor = {90, 5, 80, 40, (char*)"Sound Sensor", 0, true, 0};
    struct Render LightSensor = {175, 5, 80, 40, (char*)"Light Sensor", 0, true, 0};

    //Average
    struct Render TemperatureAvg = {5, 50, 80, 40, (char*)"Temp Average", 0, true, 0};
    struct Render SoundSensorAvg = {90, 50, 80, 40, (char*)"Sound Average", 0, true, 0};
    struct Render LightSensorAvg = {175, 50, 80, 40, (char*)"Light Average", 0, true, 0};

    //Max
    struct Render TemperatureMax = {5, 95, 80, 40, (char*)"Temp Max", 0, true, 0};
    struct Render SoundSensorMax = {90, 95, 80, 40, (char*)"Sound Max", 0, true, 0};
    struct Render LightSensorMax = {175, 95, 80, 40, (char*)"Light Max", 0, true, 0};

    //Low
    struct Render TemperatureMin = {5, 140, 80, 40, (char*)"Temp Min", 0, true, 0};
    struct Render SoundSensorMin = {90, 140, 80, 40, (char*)"Sound Min", 0, true, 0};
    struct Render LightSensorMin = {175, 140, 80, 40, (char*)"Light Min", 0, true, 0};

    struct Render Touched = {lcd.GetXSize() - 195, 5, 80, 40, (char*)"Touch Reading", (char*)"Touched", false, 0};

    struct Render BuildingCodeRender = {lcd.GetXSize() - 110, 5, 100, 40, (char*)"Building Code", 0, true, &RenderCodes};
    struct Render RoomCodeRender     = {lcd.GetXSize() - 110, 50, 100, 40, (char*)"Room Code", 0, true, &RenderCodes};
    struct Render IpRender           = {5, 200, 110, 40, (char*)"Ip Address", 0, true, 0};

    std::vector<Render> VideoInitialization = std::vector<Render>();

    //Add them to a vector for easy array size
    VideoInitialization.push_back(Temperature);
    VideoInitialization.push_back(Touched);
    VideoInitialization.push_back(SoundSensor); // 3
    VideoInitialization.push_back(LightSensor);

    VideoInitialization.push_back(BuildingCodeRender);
    VideoInitialization.push_back(RoomCodeRender); // 5
    VideoInitialization.push_back(IpRender);

    VideoInitialization.push_back(TemperatureAvg);
    VideoInitialization.push_back(SoundSensorAvg); // 8
    VideoInitialization.push_back(LightSensorAvg);

    VideoInitialization.push_back(TemperatureMax);
    VideoInitialization.push_back(SoundSensorMax); // 11
    VideoInitialization.push_back(LightSensorMax);

    VideoInitialization.push_back(TemperatureMin);
    VideoInitialization.push_back(SoundSensorMin); // 14
    VideoInitialization.push_back(LightSensorMin);

    video.AddLayer(&VideoInitialization[0], VideoInitialization.size());

    //Initial draw
    video.Draw();

#ifdef DEBUG
    logger->Log("Handing over to interrupt queue");
#endif

    Thread ethernetThread;
    ethernetThread.start(callback(&eth, &Ethernet::Initialize));

    EventQueue queue;
    //Leave the rest up to the event queues
    queue.call_every(3, Touch);
    queue.dispatch_forever();
}
